package hackathon.resource;

import hackathon.dao.DAOConnection;

import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/oauth")
public class Resource {

    @POST
	@Path("/token")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response getAccessToken(
			@FormParam("nid") String nid,
			@FormParam("client_id") String client_id,
			@FormParam("client_secret") String client_secret,
			@FormParam("code") String code,
			@FormParam("redirect_uri") String redirect_uri) {

    	System.out.println("getting access token service");
    	

		try {
			Connection connection = DAOConnection.getConnection();
		    Statement stmt = connection.createStatement();
		    ResultSet rs = null;
		    
	    	// VERIFY CLIENT_ID AND CLIENT_SECRET AND REDIRECT_URI
		    rs = stmt.executeQuery("SELECT AppName FROM App WHERE client_id='"+client_id+"' AND client_secret='"+client_secret+"' AND redirect_uri='"+redirect_uri+"'");
		    if (!rs.first()) {
		    	// THROW EXCEPTION
		    	Response response = Response.status(Status.BAD_REQUEST).entity("{\"error\": \"Wrong Client ID or SECRET OR REDIRECT URL\"}").build();
		    	connection.close();
		    	return response;
		    }
		    
	    	// VERIFY CODE EXISTS FOR A USER
		    rs = stmt.executeQuery("SELECT PersonID FROM User WHERE code='"+code+"'");
		    if (!rs.first()) {
		    	// THROW EXCEPTION
		    	Response response = Response.status(Status.BAD_REQUEST).entity("{\"error\": \"Wrong Request Token/Code\"}").build();
		    	connection.close();
		    	return response;
		    }
		    
		    //generate access_token & return
    		String access_token= UUID.randomUUID().toString();
    		// UPDATE ACCESS TOKEN INTO DB
		    stmt.executeUpdate("UPDATE User SET access_token = '"+access_token+"' WHERE code = '"+code+"'");
		    
		    Response response = Response.status(Status.OK).entity("{\"access_token\": \""+access_token+"\"}").build();
		    connection.close();
	    	return response;
   
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
    	
    	Response response = Response.status(Status.OK).build();
    	return response;
    	
	}
    
    
    
    
    

}



