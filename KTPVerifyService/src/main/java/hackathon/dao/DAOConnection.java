package hackathon.dao;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOConnection {
	
	//Connection connection = getConnection();
//    Statement stmt = connection.createStatement();
//    stmt.executeUpdate("DROP TABLE IF EXISTS ticks");
//    stmt.executeUpdate("CREATE TABLE ticks (tick timestamp)");
//    stmt.executeUpdate("INSERT INTO ticks VALUES (now())");
//    ResultSet rs = stmt.executeQuery("SELECT tick FROM ticks");
//    while (rs.next()) {
//        System.out.println("Read from DB: " + rs.getTimestamp("tick"));
//    }
    
    public static Connection getConnection() throws URISyntaxException, SQLException {
    	System.out.println("-------- MySQL JDBC Connection Testing ------------");
    	Connection connection = null;
    	
    	try {
    		Class.forName("com.mysql.jdbc.Driver");
    	} catch (ClassNotFoundException e) {
    		System.out.println("Where is your MySQL JDBC Driver?");
    		e.printStackTrace();
    		return connection;
    	}

    	System.out.println("MySQL JDBC Driver Registered!");
    	
    	try {
    		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inti_schema","root", "1234");
    	} catch (SQLException e) {
    		System.out.println("Connection Failed! Check output console");
    		e.printStackTrace();
    		return connection;
    	}

    	if (connection != null) {
    		System.out.println("You made it, take control your database now!");
    	} else {
    		System.out.println("Failed to make connection!");
    	}
        return connection;
    }

}
