CREATE TABLE Ktp
(
Nik varchar(255),
Nama varchar(255),
Kelurahan varchar(255),
Kecamatan varchar(255),
Kabupaten varchar(255),
Provinsi varchar(255),
Kelamin ENUM('laki-laki', 'perempuan'),
PRIMARY KEY (Nik)
);

CREATE TABLE User
(

PersonID int NOT NULL AUTO_INCREMENT,
Email varchar(255),
Pass varchar(255),
Nik	varchar(255),
code	varchar(255),
access_token	varchar(255),
PRIMARY KEY (PersonID),
FOREIGN KEY (Nik)
	REFERENCES Ktp (Nik)
    ON DELETE NO ACTION
);

CREATE TABLE App
(
AppName varchar(255),
client_id varchar(255),
client_secret varchar(255),
redirect_uri varchar(255)
);