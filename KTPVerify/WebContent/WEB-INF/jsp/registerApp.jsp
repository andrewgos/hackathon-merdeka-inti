<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <title>Register Your App</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="http://www.cse.unsw.edu.au/~agos568/styles.css">     
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" id="registerApp-container">
  <h2 id="pad">Register Your App</h2>
<ul class="breadcrumb bread-main">
  <form class="form-horizontal" role="form" action="oauth" method="POST">
    <div class="form-group">
      <label class="control-label col-sm-4" for="appName">Your App Name:</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="appName" name="appName" placeholder="App Name" required>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4" for="pwd">Your App Redirect URI:</label>
      <div class="col-sm-8">
        <input type="redirect_uri" class="form-control" id="redirect_uri" name="redirect_uri" placeholder="Redirect URI" required>
      </div>
    </div>
    
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
      	<a button id="no_button" type="button" href="home" class="btn btn-default" style="color:white">Home</a>
        <button id="yes_button" type="submit" name="action" value="registerApp" class="btn btn-default pull-right">Register</button>
        
      </div>
    </div>

  </form>
 
</ul>
</div>

</body>
</html>
