<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang="en">
    <head>
      <title>Pendaftaran</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="http://www.cse.unsw.edu.au/~agos568/styles.css">     
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>

    <body>
    <div class="container" id="regCont">
      <h2 id="pad_up">Pendaftaran</h2>
    <ul class="breadcrumb bread-main" id="regCrumb">
	${error}
      <form class="form-horizontal" role="form" method="POST" action="control">
      
      	<div class="form-group">
          <label class="control-label col-sm-2" for="email">Email:</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" name="email" placeholder="Masukkan email" required>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-2" for="pwd">Password:</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="pwd" placeholder="Masukkan kode sandi" required>
          </div>
        </div>
        
        <div class="form-group">
          <label class="control-label col-sm-2" for="nik">NIK:</label>
          <div class="col-sm-10">
            <input type="number" class="form-control" name="nik" placeholder="Masukkan NIK" maxlength="16" pattern="([0-9]{16})" required>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-2" for="fullname">Nama Lengkap:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="fullname" placeholder="Masukkan Nama Lengkap" required>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-2" for="kelurahan">Kelurahan:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="kelurahan" placeholder="Masukkan Kelurahan" required>
          </div>
        </div>


        <div class="form-group">
          <label class="control-label col-sm-2" for="kecamatan">Kecamatan:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="kecamatan" placeholder="Masukkan Kecamatan" required>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-2" for="kabupaten">Kabupaten:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="kabupaten" placeholder="Masukkan Kabupaten" required>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-2" for="provinsi">Provinsi:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="provinsi" placeholder="Masukkan Provinsi" required>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-2" for="sex">Jenis Kelamin:</label>
          <div class="col-sm-10">
            <div class="dropdown">
              <select name="sex" class="form-control">
                <option value="laki-laki">Laki-laki</option>
                <option value="perempuan">Perempuan</option>
              </select>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button id="yes_button" type="submit" name="action" value="register" class="btn btn-default pull-right">Konfirmasi</button>
            <a button id="no_button" type="button" href="home" class="btn btn-default" style="color:white">Kembali</a>
          </div>
        </div>

      </form>
    </ul>
    </div>

    </body>
    </html>
