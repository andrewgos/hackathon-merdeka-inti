<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang="en">
    <head>
      <title>InTI OAuth Authentication</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="http://www.cse.unsw.edu.au/~agos568/styles.css">     
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container" id="about-container">
    <h2 id="pad">OAuth Authentication with InTI</h2>
    <ul class="breadcrumb bread-main">
        1. Register your App with us<br><br>
        2. Send your user to <strong><u>http://localhost:8080/KTPVerify/oauth</u></strong> with the following query string parameters
        	<strong>client_id</strong>,  <strong>redirect_uri</strong><br><br>
        3. The user will approve your app.<br><br>
        4. The user then will be redirected to the given <strong>redirect_uri</strong> with a query string parameter
        	<strong>code</strong><br><br>
       	5. Make a POST request to <strong><u>http://localhost:8080/KTPVerifyService/oauth/token</u></strong> with these parameters:
       		<strong>client_id</strong>, <strong>client_secret</strong>, <strong>code</strong>, <strong>redirect_uri</strong><br><br>
       		
    </ul>
    <a button id="no_button" type="button" href="home" class="btn btn-default pull-right" style="color:white">Home</a>
    </div>

    </body>
    </html>
