<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang="en">
    <head>
      <title>App Details</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="http://www.cse.unsw.edu.au/~agos568/styles.css">     
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container">
      <h2 id="pad">Detil Aplikasi Anda</h2>
    <ul class="breadcrumb bread-main">
        <strong>Nama Aplikasi:</strong> ${appName}<br>
        <strong>client_id:</strong> ${client_id}<br>
        <strong>client_secret:</strong> ${client_secret}<br>
        <strong>redirect_uri:</strong> ${redirect_uri}<br>
        
                

    </ul>
        <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
      	<a button id="no_button" type="button" href="home" class="btn btn-default pull-right" style="color:white">Home</a>
        
      </div>
    </div>
    
    </div>

    </body>
    </html>
