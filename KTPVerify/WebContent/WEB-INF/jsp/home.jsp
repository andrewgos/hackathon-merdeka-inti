<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang="en">
    <head>
      <title>Login</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="http://www.cse.unsw.edu.au/~agos568/styles.css">     
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container">
      <h2 id="pad">InTI</h2>
    <ul class="breadcrumb bread-main">
      <form class="form-horizontal" role="form" action="control" method="POST">
        <div class="form-group">
          <label class="control-label col-sm-2" for="email">Email:</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan email">
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-2" for="pwd">Password:</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Masukkan kode sandi">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
              <label><input type="checkbox">Ingat saya</label>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
          <a button id="no_button" type="button" href="registration" class="btn btn-default" style="color:white">Registrasi</a>
          <button id="yes_button" type="submit" name="action" value="login" class="btn btn-default pull-right">Login</button>
          </div>
        </div>

      </form>
    </ul>
    <ol class="breadcrumb" id="breadcrumb-list">
  		<li><a href="registerApp">Register Your App</a></li>
  		<li><a href="aboutOAuth">Tentang OAuth API</a></li>
	</ol>
    </div>

    </body>
    </html>
