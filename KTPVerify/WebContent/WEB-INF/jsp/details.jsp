<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="hackathon.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <title>Pendaftaran</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="http://www.cse.unsw.edu.au/~agos568/styles.css">     
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2 id="pad_up">Detil Akun</h2>
<ul class="breadcrumb bread-main">
<c:set var="userKtp" scope="session" value="${ktp}"/>
    <form class="form-horizontal" role="form">
      <div class="form-group">
        <label class="control-label col-sm-2" for="nik">NIK:</label>
        <div class="col-sm-10">
          <input type="number" class="form-control" name="nik" value="${userKtp.nik}" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2" for="fullname">Nama Lengkap:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="fullname" value="${userKtp.name}" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2" for="kelurahan">Kelurahan:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="kelurahan" value="${userKtp.kelurahan}" readonly>
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-sm-2" for="kecamatan">Kecamatan:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="kecamatan" value="${userKtp.kecamatan}" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2" for="kabupaten">Kabupaten:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="kabupaten" value="${userKtp.kabupaten}" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2" for="provinsi">Provinsi:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="provinsi" value="${userKtp.provinsi}" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2" for="sex">Jenis Kelamin:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="provinsi" value="${userKtp.kelamin}" readonly>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <a button id="no_button" type="button" href="home" class="btn btn-default pull-right" style="color:white">Kembali</a>
          
        </div>
      </div>

    </form>
  </ul>
  </div>

  </body>
  </html>
