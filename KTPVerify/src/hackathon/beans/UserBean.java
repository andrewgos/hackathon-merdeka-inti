package hackathon.beans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * Class that represents a user of the system
 *
 * @author  $author
 */
public class UserBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The user's id in the database
	 */
	private long id;
	
	
	/**
	 * The email that is used to log in
	 */
	private String email;
	

	/**
	 * The current password of the user
	 */
	private String password;
	
	/**
	 * The email of the user
	 */
	//private String email;
	
	/**
	 * The post code of the user's address
	 */
	private Set<KTPBean> userKTP = new HashSet<KTPBean>();

	
	/** Creates a new instance of User */	
	public UserBean() {
		super();
	}
	
	public UserBean(String email, String password) {
		this();
		this.email = email;
		this.password = password;
	}

	/** Getter for property id.
	 * @return Value of property id.
	 *
	 */
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}	
	

	/** Getter for property email.
	 * @return Value of property email.
	 *
	 */
	public java.lang.String getEmail() {
		return email;
	}

	/** Setter for property email.
	 * @param username New value of property email.
	 *
	 */
	public void setEmail(java.lang.String email) {
		this.email = email;
	}
	
	/** Getter for property password.
	 * @return Value of property password.
	 *
	 */
	public java.lang.String getPassword() {
		return password;
	}
	
	/** Setter for property password.
	 * @param password New value of property password.
	 *
	 */
	public void setPassword(java.lang.String password) {
		this.password = password;
	}
	
	
	/** Getter for property ktp.
	 * @return Value of property ktp.
	 *
	 */
	public Set<KTPBean> getKtp() {
		return userKTP;
	}
	
	/** Setter for property ktp.
	 * @param password New value of property ktp.
	 *
	 */
	public void setKtp(Set<KTPBean> userKTP) {
		this.userKTP = userKTP;
	}
	
}
