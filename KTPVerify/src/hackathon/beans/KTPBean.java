package hackathon.beans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class KTPBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private UserBean owner;
	private String name;
	private String nik;
	private String kelurahan;
	private String kecamatan;
	private String kabupaten;
	private String provinsi;
	private String kelamin;
	
	private KTPBean() {
	} 

	/**
	 * Card Bean with all the fields.
	 * @param id
	 * @param name
	 * @param nik
	 * @param kelurahan
	 * @param kecamatan
	 * @param kabupaten
	 * @param provinsi
	 * @param kelamin
	 */
	public KTPBean(UserBean owner, String name, String nik, 
			String kelurahan, String kecamatan, String kabupaten, String provinsi,
			String kelamin) {
		this();
		this.setOwner(owner);
		this.name = name;
		this.nik = nik;
		this.kelurahan = kelurahan;
		this.kecamatan = kecamatan;
		this.kabupaten = kabupaten;
		this.provinsi = provinsi;
		this.kelamin = kelamin;
	}

	public UserBean getOwner() {
		return owner;
	}

	public void setOwner(UserBean owner) {
		this.owner = owner;
	}

	/** Getter for property name on card.
	 * @return Value of property name on card.
	 *
	 */
	public String getName() {
		return name;
	}
	
	/** Setter for property name on KTP.
	 * @param cardname New value of property name on KTP.
	 *
	 */
	public void setName(String cardName) {
		this.name = cardName;
	}
	
	/** Getter for property KTP NIK.
	 * @return Value of property KTP NIK.
	 *
	 */
	public String getNik() {
		return nik;
	}
	
	/** Setter for property KTP NIK.
	 * @param cardname New value of property KTP NIK.
	 *
	 */
	public void setNik(String nik) {
		this.nik = nik;
	}
	
	/** Getter for property kelurahan.
	 * @return Value of property kelurahan.
	 *
	 */
	public String getKelurahan() {
		return kelurahan;
	}
	
	/** Setter for property kelurahan.
	 * @param cardname New value of property kelurahan.
	 *
	 */
	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}
	
	/** Getter for property kecamatan.
	 * @return Value of property kecamatan.
	 *
	 */
	public String getKecamatan() {
		return kecamatan;
	}
	
	/** Setter for property kecamatan.
	 * @param cardname New value of property kecamatan.
	 *
	 */
	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}
	
	/** Getter for property kabupaten.
	 * @return Value of property kabupaten.
	 *
	 */
	public String getKabupaten() {
		return kabupaten;
	}
	
	/** Setter for property kabupaten.
	 * @param cardname New value of property kabupaten.
	 *
	 */
	public void setKabupaten(String kabupaten) {
		this.kabupaten = kabupaten;
	}
	
	/** Getter for property provinsi.
	 * @return Value of property provinsi.
	 *
	 */
	public String getProvinsi() {
		return provinsi;
	}
	
	/** Setter for property kelamin.
	 * @param cardname New value of property kelamin.
	 *
	 */
	public void setKelamin(String kelamin) {
		this.kelamin = kelamin;
	}
	/** Getter for property kelamin.
	 * @return Value of property kelamin.
	 *
	 */
	public String getKelamin() {
		return kelamin;
	}
	
	/** Setter for property provinsi.
	 * @param cardname New value of property provinsi.
	 *
	 */
	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}
	

	public long getId() {
		return id;
	}

	private void setId(long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
}