package hackathon.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class ControlServlet
 */
@WebServlet("/control")
public class ControlServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Map<String,Command> commands;
	
	/** Initializes the servlet.
	 */
	@Override
	public void init() throws ServletException {
		super.init();
		commands = new HashMap<String,Command>();
		commands.put("home", new HomeCommand());
		commands.put("register", new RegisterCommand());
		commands.put("login", new LoginCommand());
		
	}

	
	/** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
	 * @param request servlet request
	 * @param response servlet response
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		String next = null;
		Command cmd = null;
		
		cmd = resolveCommand(request);

		if (cmd == null) {
			cmd = commands.get("home");
		} 
		
		next = cmd.execute(request, response);
		System.out.println("next = " + next);
		
		if (next.indexOf('.') < 0) {
			cmd = (Command) commands.get(next);
	        	next = cmd.execute(request, response);
	    	}
	        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(next);
	        dispatcher.forward(request, response);
	}
	
	private Command resolveCommand(HttpServletRequest request) {
		String test = request.getParameter("action");
		System.out.println("COMMAND = " + test);
		Command cmd = commands.get(request.getParameter("action"));
		if (request.getParameter("action") == null || request.getParameter("action").equals("")) {
			cmd = commands.get("home");
		} 
		else if(cmd == null) {
			cmd = commands.get("PAGE_NOT_FOUND");
		}
		return cmd;
	}
	
	/** Handles the HTTP <code>GET</code> method.
	 * @param request servlet request
	 * @param response servlet response
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/** Handles the HTTP <code>POST</code> method.
	 * @param request servlet request
	 * @param response servlet response
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/** Returns a short description of the servlet.
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}	
}
