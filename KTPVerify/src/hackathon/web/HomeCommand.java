package hackathon.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeCommand implements Command {

	
	public HomeCommand() {
	}
	
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			return "/WEB-INF/jsp/home.jsp";
		} catch (Exception e) {
			request.getSession().setAttribute("error", e.getMessage());
			return "errorResult";
		}
		
	}
	
		
}

