package hackathon.web;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.http.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import hackathon.beans.KTPBean;
import hackathon.beans.UserBean;
import hackathon.dao.DAOConnection;

public class RegisterCommand implements Command {
	
	public RegisterCommand() {
	}
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("executing registercommand...");
		
		Connection conn = null;
		try {
			conn = DAOConnection.getConnection();
		} catch (URISyntaxException | SQLException e1) {
			e1.printStackTrace();
		}
		
		Map<String, String[]> userMap = new HashMap<String, String[]>(request.getParameterMap());
		
		UserBean newUser = new UserBean();
		try {
			String uEmail = userMap.get("email")[0];
			
			newUser.setEmail(uEmail);
			
			String uPass = userMap.get("pwd")[0];
			newUser.setPassword(uPass);

			String KTPName = userMap.get("fullname")[0];
			String KTPNik = userMap.get("nik")[0];
			String kelurahan = userMap.get("kelurahan")[0];
			String kecamatan = userMap.get("kecamatan")[0];
			String kabupaten = userMap.get("kabupaten")[0];
			String provinsi = userMap.get("provinsi")[0];
			String kelamin = userMap.get("sex")[0];
			
			if(!verify(KTPNik,KTPName)){
				System.out.println("VERIFICAITON FAIL");
				request.setAttribute("error", "Detil KTP tidak terdaftar");
				return "/WEB-INF/jsp/registration.jsp";
			}

			KTPBean userKTP = new KTPBean(newUser, KTPName, KTPNik, kelurahan, kecamatan, kabupaten, provinsi, kelamin);
			
			newUser.getKtp().add(userKTP);
			
			Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stmt.executeUpdate("INSERT INTO Ktp(Nik, Nama, Kelurahan, Kecamatan, Kabupaten, Provinsi, Kelamin) VALUES ( '"
								+ KTPNik + "','" + KTPName + "','" + kelurahan + "','" + kecamatan + "','"
								+ kabupaten + "','" + provinsi + "','" + kelamin + "' )");
			stmt.executeUpdate("INSERT INTO User(Email, Pass, Nik) VALUES ('" + uEmail + "','" + uPass + "','" + KTPNik + " ')");
			
			System.out.println("Registration Complete!");
			
			return "/WEB-INF/jsp/verification.jsp";
		} catch (Exception e) {
			System.out.println("Registration Fail!");
			System.out.println(e.getMessage());
			request.setAttribute("error", e.getMessage());
			return "/WEB-INF/jsp/registration.jsp";
		}
		
	}
	
	private boolean verify(String nik, String nama){
		
		DefaultHttpClient httpclient = new DefaultHttpClient();
	    try {
	      // specify the host, protocol, and port
	      HttpHost target = new HttpHost("www.kawalpilkada.id", 80, "http");
	       
	      // specify the get request
	      HttpGet getRequest = new HttpGet("/cekktp/json/"+nik);
	 
	      System.out.println("executing request to " + target);
	 
	      HttpResponse httpResponse = httpclient.execute(target, getRequest);
	      HttpEntity entity = httpResponse.getEntity();
	 
	      if (entity != null) {
	        String jsonResponse = EntityUtils.toString(entity);
	        System.out.println(jsonResponse);
	        
	        ObjectMapper mapper = new ObjectMapper();
			try {
				JsonNode rootNode = mapper.readTree(jsonResponse);
				if(jsonResponse.length() < 70){
					return false;
				}
				
				String nikAPI = rootNode.get("nik").getTextValue();
				String namaAPI = rootNode.get("nama").getTextValue();
				
				if(nik.equalsIgnoreCase(nikAPI) && nama.equalsIgnoreCase(namaAPI)){
					return true;
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
	      }
	 
	    } catch (Exception e) {
	      e.printStackTrace();
	    }

		return false;
		
	}

}
