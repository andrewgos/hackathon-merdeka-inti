package hackathon.web;

import hackathon.dao.DAOConnection;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class ControlServlet
 */
@WebServlet("/oauth")
public class OAuthServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Map<String,Command> commands;
	
	/** Initializes the servlet.
	 */
	@Override
	public void init() throws ServletException {
		super.init();
	}


	
	/** Handles the HTTP <code>GET</code> method.
	 * @param request servlet request
	 * @param response servlet response
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		System.out.println("doGET di oauthservlet...");
		

		String next ="/WEB-INF/jsp/loginOAuth.jsp";
		
		String client_id = request.getParameter("client_id");
		String redirect_uri = request.getParameter("redirect_uri");
		
		// CHECK CLIENT_ID EXISTS
		try {
			Connection connection = DAOConnection.getConnection();
		    Statement stmt = connection.createStatement();
		    ResultSet rs = stmt.executeQuery("SELECT AppName FROM App WHERE client_id='"+client_id+"'");
		    if (!rs.first()) {
		    	// THROW EXCEPTION
		        System.out.println("CLIENT ID IS WRONG");
		        next = "/WEB-INF/jsp/errorOAuth.jsp";
		        request.setAttribute("error", "Valid client_id is required as parameter");
		    }
		    else{
		    	String appName = rs.getString("AppName");
				request.setAttribute("appName", appName);
				System.out.println(redirect_uri);
				request.setAttribute("redirect_uri", redirect_uri);		    	
		    }
		    

		    connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(next);
        dispatcher.forward(request, response);
		
		return;
	}
	
	/** Handles the HTTP <code>POST</code> method.
	 * @param request servlet request
	 * @param response servlet response
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		System.out.println("...POST di oauthservlet...");
		
		String next = "/WEB-INF/jsp/home.jsp";
		
		String action = request.getParameter("action");
		
		if(action.equalsIgnoreCase("login")){
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String redirect_uri = request.getParameter("redirect_uri");
			String appName = request.getParameter("appName");

			// verify if username is correct
			try {
				Connection connection = DAOConnection.getConnection();
			    Statement stmt = connection.createStatement();
			    ResultSet rs = stmt.executeQuery("SELECT PersonID FROM User WHERE Email='"+email+"' AND Pass='"+password+"'");
			    if (!rs.first()) {
			    	// THROW EXCEPTION
			    	request.setAttribute("error", "login failed.");
			    	request.setAttribute("appName", appName);
			    	next = "/WEB-INF/jsp/loginOAuth.jsp";
			    }
			    else{
			    	//IF SUCCESS
					String code = UUID.randomUUID().toString();
					
					System.out.println(rs.getInt("PersonID"));
			    	int PersonID = rs.getInt("PersonID");
					
					
					// UPDATE DB USER FOR CODE
					try {
					    stmt.executeUpdate("UPDATE User SET code = '"+code+"' WHERE PersonID = '"+PersonID+"'");
					    connection.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
					
					
					next = redirect_uri+"?code="+code;
			        response.sendRedirect(next);
					return;
			    }

			    connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			
				
		}
		else if(action.equalsIgnoreCase("registerApp")){
			System.out.println("...registering your app...");
			
			String appName = request.getParameter("appName");
			String redirect_uri = request.getParameter("redirect_uri");
			
			String client_id = UUID.randomUUID().toString();
			String client_secret = UUID.randomUUID().toString();
			
			// INSERTING INTO DB
			try {
				Connection connection = DAOConnection.getConnection();
			    Statement stmt = connection.createStatement();
			    stmt.executeUpdate("INSERT INTO App (AppName,client_id,client_secret,redirect_uri) VALUES ('"+appName+"','"+client_id+"','"+client_secret+"','"+redirect_uri+"')");
			    connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			
			request.setAttribute("appName", appName);
			request.setAttribute("client_id", client_id);
			request.setAttribute("client_secret", client_secret);
			request.setAttribute("redirect_uri", redirect_uri);
			next = "/WEB-INF/jsp/showApp.jsp";
		
		}

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(next);
        dispatcher.forward(request, response);
		return;
	}
}
