package hackathon.web;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hackathon.beans.KTPBean;
import hackathon.beans.UserBean;
import hackathon.dao.DAOConnection;


public class LoginCommand implements Command {

	
	public LoginCommand() {
	} 
	
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {	
		
		try {
			
			Connection connection = null;
			connection = DAOConnection.getConnection();
			
			Statement stmt;
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			String uEmail = request.getParameter("email");
			String uPass = request.getParameter("pwd"); 
			 ResultSet user = stmt.executeQuery("SELECT * FROM User WHERE Email = '" + uEmail + "' AND Pass = '" + uPass + "'");
			    
		    if (!user.first()) {
		        // THROW EXCEPTION
		    	request.setAttribute("error", "Invalid Username or Password");
				System.out.println("MAAF LOGIN GAGAL");
				return "/WEB-INF/jsp/home.jsp";
		    }
		    else {
		    	String uNik = user.getString("Nik");
		    	ResultSet dataKtp = stmt.executeQuery("SELECT * FROM Ktp WHERE Nik = '" + uNik + "'");
	
		    	UserBean owner = new UserBean(uEmail, uPass);
		    	
		    	if (dataKtp.first()) {
		    		String dNik = dataKtp.getString("Nik");
		    		String dNama = dataKtp.getString("Nama");
		    		String dKel = dataKtp.getString("Kelurahan");
		    		String dKec = dataKtp.getString("Kecamatan");
		    		String dKab = dataKtp.getString("Kabupaten");
		    		String dProv = dataKtp.getString("Provinsi");
		    		String dSex = dataKtp.getString("Kelamin");
		    		KTPBean data = new KTPBean(owner, dNama, dNik, dKel, dKec, dKab, dProv, dSex);
		    	
		    		request.setAttribute("ktp", data);
		    	}
		    }
		    
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		System.out.println("LOGIN SUKSES");
		return "/WEB-INF/jsp/details.jsp";
	}

}

